#### ﻿gcd 最大公约数

**分解质因数法**

找最小公约数，一直约直到没有最小公约数，然后将最小公约数相乘
缺点：时间复杂度高，计算复杂

**辗转相除法（欧几里德算法）**

```java
int gcd(int a, int b) {
	if (b == 0) return a;
	return gcd(b,a%b);
}
```

> 时间复杂度O(logn)
> 如果两个数特别大，模运算性能不高

**更相减损术**

较大的数减去较小的数

> gcd(a-b, b);
> 比如：gcd(42,30) = 6
> 42-1*30 = 12          30-2*12 = 6
>
> 更相减损术计算来看，数字大的减去小的
> gcd(42,30) = gcd(12, 30) = gcd(18, 12) = gcd(6, 12) = gcd(6, 6)
> 					      两数相等，已得出最大公约数

递归方式

```java
int gcd(int a, int b){
	if(a==b) return a;
	return a>b? gcd(a-b,b) : gcd(b-a,a);
}
```

时间复杂度最坏为O(n)
两数相差悬殊时， 10000 和 1 需要递归9999次

所以将两者相结合
分析4种情况

1. a偶数， b偶数：都有相同公因数2，先将2除掉，计算完再还回去，gcd(a/2,b/2)*2
2. a偶数， b奇数：gcd(a/2,b);
3. a奇数， b偶数：gcd(a,b/2);
4. a奇数， b奇数：使用更相减损术，分两种情况
	a>b gcd(a-b,b);
	a<b gcd(b-a,a);

时间复杂度为O(logn)