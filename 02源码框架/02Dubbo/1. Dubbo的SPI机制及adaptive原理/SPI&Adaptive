## SPI（Service Provider Interface）

Dubbo的SPI对比Java SPI，Dubbo对服务发现机制做了增强。添加了一些默认的加载机制等等。

#### 三种注入实例方式

> AdaptiveExtensionFactory
> SpiExtensionFactory
> SpringExtensionFactory

```java
public class ExtensionLoader<T> {

    private static final String DUBBO_DIRECTORY = "META-INF/dubbo/";
	// 默认加载路径
    private static final String DUBBO_INTERNAL_DIRECTORY = DUBBO_DIRECTORY + "internal/";
    // 根据路径获取拓展类
    public T getExtension(String name) {}
	/*  1.通过 getExtensionClasses 获取所有的拓展类
       *2.通过反射创建拓展对象
       *3.向拓展对象中注入依赖
       *4.将拓展对象包裹在相应的 Wrapper 对象中*/
    private T createExtension(String name) {}
	// 获取所有拓展类        
    private Map<String, Class<?>> getExtensionClasses() {}
    /*  1.对 SPI 注解进行解析，
       *2.调用 loadDirectory 方法加载指定文件夹配置文件
       */
    private Map<String, Class<?>> loadExtensionClasses() {}
    // 通过 classLoader 获取所有资源链接，然后再通过 loadResource 方法加载资源
    private void loadDirectory(Map<String, Class<?>> extensionClasses, String dir) {}
    // 读取和解析配置文件，并通过反射加载类，最后调用 loadClass 方法进行其他操作
    private void loadResource(Map<String, Class<?>> extensionClasses, 
	ClassLoader classLoader, java.net.URL resourceURL) {}
```

#### Dubbo SPI特点

> META-INF.services中配置的文件(文件名为接口全名)，可以配置多个，加载通过name的方式获取
>
> 如果加载类中需要依赖其他类，通过IOC注入实例

## Activate

> 如果方法中调用的是父类或接口层面的方法，如何找到对应的具体实现类，通过配置@Activate注解，然后通过传入的URL属性，找到对应的实现类
>
> @Activate value=属性，map.put("属性","SPI的name")





## Dubbo IOC

```java
private T injectExtension(T instance) {
    try {
        if (objectFactory != null) {
            // 遍历目标类的所有方法
            for (Method method : instance.getClass().getMethods()) {
                // 检测方法是否以 set 开头，且方法仅有一个参数，且方法访问级别为 public
                if (method.getName().startsWith("set")
                    && method.getParameterTypes().length == 1
                    && Modifier.isPublic(method.getModifiers())) {
                    // 获取 setter 方法参数类型
                    Class<?> pt = method.getParameterTypes()[0];
                    try {
                        // 获取属性名，比如 setName 方法对应属性名 name
                        String property = method.getName().length() > 3 ? 
                            method.getName().substring(3, 4).toLowerCase() + 
                            	method.getName().substring(4) : "";
                        // 从 ObjectFactory 中获取依赖对象
                        Object object = objectFactory.getExtension(pt, property);
                        if (object != null) {
                            // 通过反射调用 setter 方法设置依赖
                            method.invoke(instance, object);
                        }
                    } catch (Exception e) {
                        logger.error("fail to inject via method...");
                    }
                }
            }
        }
    } catch (Exception e) {
        logger.error(e.getMessage(), e);
    }
    return instance;
}
```

