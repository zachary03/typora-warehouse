## 1. ConcurrentLinkedQueue 非堵塞队列

ConcurrentLinkedQueue:是一个适用于高并发场景下的队列，通过无所的方式，实现了高并发状态下的高性能，通常ConcurrentLinkedQueue性能好于BlockingQueue。他是一个基于连接节点的无界线程安全队列。该队列的线程遵循FIFO的原则，不允许null元素。

add 和offer() 都是加入元素的方法(在ConcurrentLinkedQueue中这俩个方法没有任何区别)
poll() 和peek() 都是取头元素节点，区别在于前者会删除元素，后者不会。

ConcurrentLinkedQueue文档说明：

| 构造方法摘要                                                 |
| ------------------------------------------------------------ |
| ConcurrentLinkedQueue()`       创建一个最初为空的 `ConcurrentLinkedQueue。 |
| ConcurrentLinkedQueue(Collection<? extends E> c)`       创建一个最初包含给定 collection 元素的 `ConcurrentLinkedQueue，按照此 collection 迭代器的遍历顺序来添加元素。 |

| 方法摘要   |                                                              |
| ---------- | ------------------------------------------------------------ |
| ` boolean` | add(E e)       将指定元素插入此队列的尾部。                  |
| ` boolean` | contains(Object o) 如果此队列包含指定元素，则返回 true。     |
| ` boolean` | isEmpty()    如果此队列不包含任何元素，则返回 true。         |
| `boolean`  | offer(E e)       将指定元素插入此队列的尾部。                |
| E          | poll()     获取并移除此队列的头，如果此队列为空，则返回 `null`。 |
| ` boolean` | remove(Object o)  从队列中移除指定元素的单个实例（如果存在）。 |

```java
public boolean offer(E e) {
    //e为null则抛出空指针异常
    checkNotNull(e);
 
   //构造Node节点构造函数内部调用unsafe.putObject，后面统一讲
    final Node<E> newNode = new Node<E>(e);
 
    //从尾节点插入
    for (Node<E> t = tail, p = t;;) {
 
        Node<E> q = p.next;
 
        //如果q=null说明p是尾节点则插入
        if (q == null) {
 
            //cas插入（1）
            if (p.casNext(null, newNode)) {
                //cas成功说明新增节点已经被放入链表，然后设置当前尾节点（包含head，1，3，5.。。个节点为尾节点）
                if (p != t) // hop two nodes at a time
                    casTail(t, newNode);  // Failure is OK.
                return true;
            }
            // Lost CAS race to another thread; re-read next
        }
        else if (p == q)//(2)
            //多线程操作时候，由于poll时候会把老的head变为自引用，然后head的next变为新head，所以这里需要
            //重新找新的head，因为新的head后面的节点才是激活的节点
            p = (t != (t = tail)) ? t : head;
        else
            // 寻找尾节点(3)
            p = (p != t && t != (t = tail)) ? t : q;
    }
}
```

# 2. ConcurrentLinkedQueue的特性

1、应用场景
按照适用的并发强度从低到高排列如下：
LinkedList/ArrayList  非线程安全，不能用于并发场景（List的方法支持栈和队列的操作，因此可以用List封装成stack和queue）
Collections.synchronizedList  使用wrapper class封装，每个方法都用synchronized(mutex:Object)做了同步
LinkedBlockingQueue  采用了锁分离的设计，避免了读/写操作冲突，且自动负载均衡，可以有界。BlockingQueue在生产-消费模式下首选【Iterator安全，不保证数据一致性】
ConcurrentLinkedQueue  适用于高并发读写操作，理论上有最高的吞吐量，无界，不保证数据访问实时一致性，Iterator不抛出并发修改异常，采用CAS机制实现无锁访问。
综上：
在并发的场景下，如果并发强度较小，性能要求不苛刻，且锁可控的场景下，可使用Collections.synchronizedList，既保证了数据一致又保证了线程安全，性能够用；
在大部分高并发场景下，建议使用 LinkedBlockingQueue ，性能与 ConcurrentLinkedQueue 接近，且能保证数据一致性；
ConcurrentLinkedQueue 适用于超高并发的场景，但是需要针对数据不一致采取一些措施。

2、特点
2.1 访问操作采用了无锁设计
2.2 Iterator的弱一致性，即不保证Iteartor访问数据的实时一致性（与current组的成员与COW成员类似）
2.3 并发poll
2.4 并发add
2.5 poll/add并发

3、注意事项
3.1 size操作不是一个固定时长的操作（not a constant-time operation）
因为size需要遍历整个queue，如果此时queue正在被修改，size可能返回不准确的数值（仍然是无法保证数据一致性），就像concurrentHashMap一样，
要获取size，需要取得所有的bucket的锁，这是一个非常耗时的操作。因此如果需要保证数据一致性，频繁获取集合对象的size，最好不使用concurrent
族的成员。

3.2 批量操作（bulk operations like addAll,removeAll,equals）无法保证原子性，因为不保证实时性，且没有使用独占锁的设计。
例如，在执行addAll的同时，有另外一个线程通过Iterator在遍历，则遍历的线程可能只看到一部分新增的数据。

3.3 ConcurrentLinkedQueue 没有实现BlockingQueue接口
当队列为空时，take方法返回null，此时consumer会需要处理这个情况，consumer会循环调用take来保证及时获取数据，此为busy waiting，会持续消耗CPU资源。

4、与 LinkedBlockingQueue 的对比
LinkedBlockingQueue 采用了锁分离的设计，put、get锁分离，保证两种操作的并发，但同一种操作，然后是锁控制的。并且当队列为空/满时，某种操作
会被挂起。

4.1 并发性能
4.1.1 高并发put操作
可支持高并发场景下，多线程无锁put操作
4.1.2 高并发的put/poll操作
多线程场景，同时put，遍历，以及poll，均可无锁操作。但不保证遍历的实时一致性。

4.2 数据的实时一致性
两者的Iterator都不不保证数据一致性，Iterator遍历的是Iterator创建时已存在的节点，创建后的修改不保证能反应出来。

4.3 遍历操作(Iterator的遍历操作的差异)
目前看来，没有差异

4.4 size操作
LinkedBlockingQueue 的size是在内部用一个AtomicInteger保存，执行size操作直接获取此原子量的当前值，时间复杂度O(1)。
ConcurrentLinkedQueue 的size操作需要遍历（traverse the queue），因此比较耗时，时间复杂度至少为O(n),建议使用isEmpty()。
The java doc says the size() method is typically not very useful in concurrent applications.

5.LinkedBlockingQueue和ConcurrentLinkedQueue适用场景

适用阻塞队列的好处：多线程操作共同的队列时不需要额外的同步，另外就是队列会自动平衡负载，即那边（生产与消费两边）处理快了就会被阻塞掉，从而减少两边的处理速度差距。
当许多线程共享访问一个公共 collection 时，ConcurrentLinkedQueue 是一个恰当的选择。

LinkedBlockingQueue 多用于任务队列

ConcurrentLinkedQueue  多用于消息队列